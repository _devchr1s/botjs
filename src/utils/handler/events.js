module.exports.events = function (fs, client, MessageEmbed, Util, Permissions) {
    //lectura de los archivos de eventos
    const eventFiles = fs.readdirSync(__dirname + "/" + `../../events/`).filter((file) => file.endsWith(".js"));
    for (const file of eventFiles) {
        //lectura del archivo
        const event = require(__dirname + "/" + `../../events/${file}`);
        //pregunta si el evento se ejecutara una vez, si no, quiere decir que se puede ejecutar varias veces
        if (event.once) {
            //le envia las propiedades obtenidas del mismo evento y unas extras
            client.once(event.name, (...args) => event.execute(...args, client));
        } else {
            //le envia las propiedades obtenidas del mismo evento y unas extras
            client.on(event.name, (...args) => event.execute(...args, client, MessageEmbed, Util, Permissions));
        }
    }
};