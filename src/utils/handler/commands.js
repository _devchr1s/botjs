const ascii = require("ascii-table"); //tabla para representar los comandos cargados en la consola

let table = new ascii("Commands"); //instancia de la tabla y titulo principal
table.setHeading("Command", "Load status"); //sub-titulos de la tabla

module.exports.commands = function (fs, client, Collection) {
    client.commands = new Collection(); //por medio del metodo Collection, creamos colecciones
    client.aliases = new Collection(); //para guardar los comandos, alias


    //las categorias se guardan dependiendo cuantas carpetas haya en la dirección correspondiente
    //es decir se carga automaticamente    
    const commandFolder = fs.readdirSync(__dirname + "/" + `../../commands`);
    client.categories = commandFolder;

    //se recorre las carpetas para obtener las subcategorias
    for (const folder of commandFolder) {
        //se lee la ruta para luego obtener los comandos
        const commandFile = fs.readdirSync(__dirname + "/" + `../../commands/${folder}`);
        for (const file of commandFile) {
            //lectura del archivo
            const command = require(__dirname + "/" + `../../commands/${folder}/${file}`);
            //verifica si el archivo existe y si tiene la propiedad name
            if (command.name) {
                //guarda el comando en discord y añade el informativo a la tabla para representarlo en consola
                client.commands.set(command.name, command);
                table.addRow(file, '✅');
            } else {
                table.addRow(file, `❌  -> ocurrio un error cargando ${file}.`);
                continue;
            }
            //pregunta si el archivo tiene alias para los comandos para luego añadirlos como alias de los comandos
            if (command.aliases && Array.isArray(command.aliases)) command.aliases.forEach(alias => client.aliases.set(alias, command.name));
        }
    }
    //representacion de la tabla en la consola
    console.log(table.toString());
};