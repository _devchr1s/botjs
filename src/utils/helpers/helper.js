module.exports = {
    //comando para añadir multiples estados al bot por cada intervalo de tiempo
    setPresence(client) {
        //definimos un array de actividades
        //por medio de los simbolos `` se puede interpolar, es decir, escribir texto y variables
        //de javascript
        const activities = [
            `${client.guilds.cache.size} servidores`,
            `${client.users.cache.size} usuarios`
        ];

        //definimos los tipos de actividades
        const types = ["WATCHING", "PLAYING", "LISTENING", "COMPETING"];

        //utilizamos la variable de actividades y para sacar un numero aleatorio
        //usamos la formula de redondear numero y dentro ejecutamos la operacion
        //numero aleatorio, nos dara un numero de 0, 0.1,0.2 hasta uno el cual
        //vamos a multiplicar por el tamaño del array, asi nos dara un numero
        //aleatorio dependiendo cuantos elementos tenga un array
        const rmdA = activities[Math.floor(Math.random() * activities.length)];
        const rmdB = types[Math.floor(Math.random() * types.length)];
        //se define el estado del bot
        client.user.setStatus('dnd');
        //se define la actividad del bot
        client.user.setActivity(rmdA, { type: rmdB });
        //impresion en consola para ver que hace el bot
        console.log('presence changed', rmdA, rmdB)
    },
    capitalizeFirstLetter(string) {
        //retorna un string con la primera letra en mayuscula
        return string.charAt(0).toUpperCase() + string.slice(1);
    },
    delay: (msec) => {
        //una funcion para hacer un retraso de algo en especifico, recibe
        //los milisegundos que quieres que se quede dormido el codigo
        new Promise((resolve) => setTimeout(resolve, msec))
    }
}