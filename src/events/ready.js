//se llama el metodo para luego utilizarse
const { setPresence } = require('../utils/helpers/helper')

module.exports = {
    //nombre del evento
    name: 'ready',
    //validacion para preguntar si se ejecuta una vez o no
    once: true,
    //funcion que dará pie cuando el bot lo solicite
    execute(client) {
        console.log(`Iniciado como ${client.user.username}`);
        //llamara la funcion cada intervalo de tiempo y le envio de propiedad client, que es necesario
        //para ejecutar funciones que el metodo solicita
        setInterval(() => {
            setPresence(client);
        }, 20000);
    },
};