//prefix del bot para los comandos
const prefix = "dz! ";

module.exports = {
    //nombre del evento
    name: 'messageCreate',
    execute(message, client, MessageEmbed, Util, Permissions) {
        //valida que si el autor del mensaje es un bot, no lo deje pasar
        if (message.author.bot) return;
        //valida que si el mensaje fue enviado al dm del bot, no lo deje pasar
        if (message.channel.type === "dm") return;
        //valida si el mensaje inicia con el prefix del bot y si el mensaje no lo envia el mismo bot
        //ya que si el mensaje lo envia el mismo bot, se crea un bucle
        if (!message.content.startsWith(prefix) || message.author.bot) return;
        //hace un split del mensaje para obtener comando, prefix y argumentos
        const args = message.content.slice(prefix.length).split(/ +/);
        //obtencion del nombre del comando
        const commandName = args.shift().toLowerCase();
        //pregunta si en la lista de comandos existe el comando solicitado o el alias solicitado
        const cmd = client.commands.find((c) => c.name === commandName || c.alias && c.alias.includes(commandName));
        if (!cmd) return;
        try {
            //ejecuta el comando
            cmd.execute(message, args, MessageEmbed, Util, client, Permissions)
        } catch (error) {
            //manejo del comando en caso de error, envia mensaje a la consola y al canal donde fue solicitado el 
            //comando
            console.log(error)
            message.channel.send("Ocurrió un error al ejecutar ese comando")
        }
    },
};