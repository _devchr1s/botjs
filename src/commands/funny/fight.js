const { delay } = require("../../utils/helpers/helper");

module.exports = {
    name: 'fight',
    category: "funny",
    alias: ["trompa", "muñequera", "machetera"],
    description: "Comando para encenderse a muñeca",
    usage: "[commando | alias] @mention",
    async execute(message, args, MessageEmbed, Util, client, Permissions) {
        const mention = message.mentions.members.first();
        if (!mention) return message.reply('¿Vas a pelear contigo mismo?¿Que eres marica?');
        
        
        
        const imgs = [
            { msg: 'PELEA DE TITANES', url: 'https://c.tenor.com/REUA-kW4UWYAAAAd/sasuke-naruto.gif' },
            { msg: 'La pelea está tensa', url: 'https://c.tenor.com/mDGzOeN100kAAAAC/kick.gif' },
            { msg: '¡YA DEJALO, ESTA MUERTO!', url: 'https://c.tenor.com/Cgfyoq2nu5kAAAAC/killua-zoldyck-hunter-x-hunter.gif' },
            { msg: 'Cule muñequera, tienes el ojo colombiano #user', url: 'https://c.tenor.com/MPPHbillO_0AAAAC/naruto-fight.gif' },
            { msg: 'erda mi llave vas peando abajo #user', url: 'https://c.tenor.com/Y_4_saaGYwUAAAAC/jujutsu-kaisen-mahito.gif' }
        ];
        let indexImg = Math.floor(Math.random() * imgs.length);
        const rmdImg = imgs[indexImg];
        const fighters = [`${message.author.username}`, `${mention.user.username}`];

        const embed = new MessageEmbed()
                            .setTitle('¡Se formó la murga!')
                            .setDescription(`***${message.author.username} ***vs*** ${mention.user.username}***`)
                            .addField('------', 
                                    `${rmdImg.msg.replace('#user', 
                                                    fighters[Math.floor(Math.random() * fighters.length)])} \n **------**`)
                            .setImage(rmdImg.url)
                            .setColor('FAE802');

        let msgSend;
        await message.reply({ embeds: [embed] }).then(msg => msgSend = msg);

        let counter = 0;
        let imgs2 = imgs;
        let interval = setInterval(() => {
            if (counter <= 2) {
                imgs2.splice(indexImg, 0);
                indexImg = Math.floor(Math.random() * imgs2.length);
                const newMsg = imgs2[indexImg];
                embed.addField('------', `${newMsg.msg.replace('#user', fighters[Math.floor(Math.random() * fighters.length)])} \n **------**`)
                    .setImage(newMsg.url);
                msgSend.edit({ embeds: [embed] });
                counter++;
            } else {
                
                const winner = fighters[Math.floor(Math.random() * fighters.length)];
                const winnerImg = ['https://c.tenor.com/pwV6FRcejvgAAAAC/killua.gif', 'https://c.tenor.com/iR3Wf_HLMYAAAAAC/laugh-lol.gif']

                const embedWinner = new MessageEmbed()
                    .setTitle('¡TENEMOS UN GANADOR!')
                    .setDescription(`El ganador es...**${winner}**`)
                    .setImage(winnerImg[Math.floor(Math.random() * winnerImg.length)])
                    .setColor('FAE802');
                msgSend.reply({ embeds: [embedWinner] }).then((reply) => {
                    setTimeout(() => {
                        reply.delete();
                        msgSend.delete();
                        message.delete();
                    }, 5000);
                })
                clearInterval(interval);
            }
        }, 5000)
    },
};