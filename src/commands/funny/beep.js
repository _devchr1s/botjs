module.exports = {
    name: 'beep',
    category: "funny",
    description: "Comando de ayuda para ver los comandos",
    usage: "[commando | alias]",
    async execute(interaction) {
        return interaction.reply('Boop!');
    },
};