const {capitalizeFirstLetter} = require('../../utils/helpers/helper');

module.exports = {
    name: "help",
    aliases: ["h"],
    category: "utilities",
    description: "Comando de ayuda para ver los comandos",
    usage: "[commando | alias]",
    execute: async (message, args, MessageEmbed, Util, client) => {

        const embed = new MessageEmbed()
            .setAuthor(client.user.username, client.user.avatarURL())
            .setFooter(message.guild.name, message.guild.iconURL)
            .setColor([209, 66, 245])

        if (args[1]) {
            return getCMD(client, message, args[1], embed);
        } else if (args[0]) {
            return getCategoryCMD(client, message, args[0], embed);
        } else {
            return getAll(client, message, embed);
        }
    }
}

function getAll(client, message, embed) {
    const info = 'Lista de comandos :hash: \n' + client.categories
        .map(cat => `**${capitalizeFirstLetter(cat)}** \n  \` ${commandsCount(cat, client)} comandos \` \n`)
        .reduce((string, category) => string + "\n" + category);

    return message.channel.send({ embeds: [embed.setDescription(info)]});
}

function commandsCount(category, client) {
    let count = 0;

    client.commands
        .filter(cmd => cmd.category === category)
        .map(cmd => count++);

    return count
}

function commands(category, client) {
    return client.commands
        .filter(cmd => cmd.category === category)
        .map(cmd => ` \` ¬ ${cmd.name}\``)
        .join("\n");
}

function getCategoryCMD(client, message, input, embed) {

    input = input.toLowerCase();

    let count = commandsCount(input, client);
    let info;

    if (count > 0) {
        info = 'Lista de comandos :hash: \n' + ` **${capitalizeFirstLetter(input)}** \n${commands(input, client)}`;
    } else {
        info = ' \`Este comando no tiene categorias/subcategorias \`'
    }


    return message.channel.send({ embeds: [embed.setDescription(info)] });

}

function getCMD(client, message, input, embed) {
    const cmd = client.commands.get(input.toLowerCase()) || client.commands.get(client.aliases.get(input.toLowerCase()));

    let info = `No information found for command **${input.toLowerCase()}**`;

    if (!cmd) {
        return message.channel.send(embed.setColor("RED").setDescription(info));
    }

    if (cmd.name) info = `**Nombre del comando**: ${cmd.name}`;
    if (cmd.aliases) info += `\n**Alias**: ${cmd.aliases.map(a => `\`${a}\``).join(", ")}`;
    if (cmd.description) info += `\n**Descripcion**: ${cmd.description}`;
    if (cmd.usage) {
        info += `\n**Modo de uso**: ${input} ${cmd.usage}`;
        embed.setFooter(`Ayuda: <> = obligatorio, [] = opcional`);
    }

    return message.channel.send({ embeds: [embed.setColor("GREEN").setDescription(info)] });
}
