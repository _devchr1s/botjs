
module.exports = {
    name: 'serverinfo',
    category: "utilities",
    description: "Información del servidor",
    usage: "[commando | alias]",
    async execute(message, args, MessageEmbed, Util, client) {
        /* const embed = new MessageEmbed()
            .setTitle('Hola mundo')
            .setDescription(message.author.username)
            .setColor("GREEN")
        message.channel.send({ embeds: [embed] }); */
        const region = {
            europe: "Europa :flag_eu:",
            brazil: "Brazil :flag_br:",
            hongkong: "Hong Kong :flag_hk:",
            japan: "Japan :flag_jp:",
            rusia: "Russia :flag_ru:",
            singapore: "Singapore :flag_sg:",
            southafrica: "South Africa :flag_za:",
            sydney: "Sydney :flag_au:",
            "us-central": "US Central :flag_us:",
            "us-east": "Este US :flag_us:",
            "us-south": "Sur US :flag_us:",
            "us-west": "Oeste US :flag_us:",
            "vip-us-east": "VIP Este US :flag_us:",
            "eu-central": "Europa Central :flag_eu:",
            "eu-west": "Europa Oeste :flag_eu:",
            london: "Europa :flag_gb:",
            amsterdam: "Amsterdam :flag_nl:",
            india: "India :flag_in:"
        };

        const verify = {
            NONE: 'Ningúno',
            LOW : 'Bajo',
            MEDIUM: 'Medio',
            HIGH: 'Alto',
            VERY_HIGH: 'Muy alto'
        };

        const server_date = message.guild.createdAt.toDateString();
        const embed = new MessageEmbed()
                            .setThumbnail(message.guild.iconURL({ dynamic: true }))
                            .setColor('FAE802')
                            .setAuthor("Información del servidor")
                            .addFields(
                                { name: ":hash: | Nombre", value: `**${message.guild.name}**`, inline: true },
                                { name: ":id: | ID", value: message.guild.id, inline: true },
                                { name: ":calendar: Fecha de creación", value: server_date },
                                { name: ":beginner: | Miembros", value: `${message.guild.memberCount} miembros`, inline: true },
                                { name: ":robot: Bots", value: `${message.guild.members.cache.filter(m => m.user.bot).size} bots`, inline: true },
                                { name: ":art: Roles", value: `${message.guild.roles.cache.size} roles  `, inline: true },
                                { name: ":crown: Owner", value: `<@${message.guild.ownerId}>` }
                            )
                            .setTimestamp()
        if (message.guild.bannerURL() !== null && message.guild.bannerURL() !== '') {
            embed.addField(":milky_way: Banner", '\u200B')
            embed.setImage(message.guild.bannerURL())
        }
        message.reply({embeds: [embed]});
    },
};