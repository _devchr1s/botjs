module.exports = {
    name: "purge",
    alias: ["pr"],
    category: "Config",
    description: "Elimina los mensajes de un canal",
    usage: "<amount>",

    execute: async (message, args, MessageEmbed, Util, client, Permissions) => {

        const embed = new MessageEmbed()
                                .setAuthor(client.user.username, client.user.avatarURL())
                                .setFooter(message.guild.name, message.guild.iconURL)
                                .setColor([209, 66, 245])


        if (!message.member.permissions.has(Permissions.FLAGS.ADMINISTRATOR)) {
            embed.setDescription(':woozy_face: Ups, no puedes hacer eso.')
            embed.setColor('RED')
            return message.channel.send({ embeds: [embed] })
        }

        if (!args[0] || isNaN(args[0])) return message.channel.send('Debes especificar un numero').then(msg => msg.delete({ timeout: 10000 }))
        if (parseInt(args[0]) > 99 || parseInt(args[0]) < 1) return message.channel.send('La cantidad maxima para eliminar es de 99 y minima 1').then(msg => msg.delete({ timeout: 10000 }))

        try {
            await message.channel.bulkDelete(parseInt(args[0]) + 1)
            message.channel.send(`Se eliminaron ${args[0]} mensajes.`).then(msg => {
                setTimeout(() => {
                    msg.delete()
                }, 5000)
            });
        } catch (error) {
            message.channel.send(`No se pudieron eliminar los mensajes, intenta luego.`).then(msg => {
                setTimeout(() => {
                    msg.delete()
                }, 5000)
            });
        }
            

    }

}