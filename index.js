const { Client, Intents, Collection, MessageEmbed, Util, Permissions } = require('discord.js'); //libreria de discord.js
const fs = require('fs'); //esta libreria sirve para leer los archivos
require('dotenv').config(); //para cargar los archivos .env y cargar variables ocultas
const client = new Client({
    intents: [Intents.FLAGS.GUILDS, Intents.FLAGS.GUILD_MESSAGES]
}); //inicalización de la clase cliente en la version 13 de discord.js

const { commands } = require(`${__dirname}/src/utils/handler/commands.js`); //manejador de comandos por evento
const { events } = require(`${__dirname}/src/utils/handler/events.js`); //manejador de eventos

commands(fs, client, Collection) //uso del metodo y las propiedades correspondientes
events(fs, client, MessageEmbed, Util, Permissions) //uso de los eventos y las propiedades correspondientes

client.login(process.env.TOKEN); //logeo del bot